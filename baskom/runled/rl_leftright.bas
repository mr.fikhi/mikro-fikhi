$regfile = "m16def.dat"
$crystal = 16000000

'$sim

Config Portd = Output
Dim Datarun As Byte
Dim X As Integer

Do

Datarun = &HFE
For X = 0 To 6
Portd = Datarun
Waitms 50
Rotate Datarun , Left
Next

For X = 0 To 6
Portd = Datarun
Waitms 50
Rotate Datarun , Right
Next

Loop
End
