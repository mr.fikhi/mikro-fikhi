$regfile = "m16def.dat"
$crystal = 16000000

'$sim

Config Portd = Output
Dim Datahigh As Byte
Dim Datalow As Byte
Dim Datarun As Byte
Dim X As Integer

Do

Datahigh = &HEF
Datalow = &HF7

For X = 0 To 3
Datarun = Datahigh And Datalow
Portd = Datarun
Waitms 100
Rotate Datahigh , Left
Rotate Datalow , Right
Next

Loop
End
