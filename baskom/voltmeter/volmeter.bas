$regfile = "m16def.dat"
$crystal = 16000000

'setting base array dan adc
Config Base = 0
Config Adc = Single , Prescaler = Auto
Start Adc

'setting lcd
Config Lcdpin = Pin , Db4 = Portc.4 , Db5 = Portc.5 , Db6 = Portc.6 , Db7 = Portc.7 , E = Portc.2 , Rs = Portc.1
Config Lcd = 20 * 4
Cursor Off Noblink
Cls

'definisi variable
Dim X As Integer
Dim X2 As Integer
Dim Y As Integer
Dim Data_adc(8) As Word
Dim Data_volt(8) As Single
Dim Str_volt(8) As String * 5

Do
'menampilkan CH dan V
For X = 1 To 4
X2 = X - 1
Y = X2 + 4
Locate X , 1 : Lcd "CH" ; X2 ; "=    V"
Locate X , 12 : Lcd "CH" ; Y ; "=    V"
Next

'get data ADC dan data 0 -> 1
For X = 0 To 7
Data_adc(x) = Getadc(x)
Data_volt(x) = Data_adc(x) / 1023
Next

'konversi ke tegangan asli
Data_volt(0) = 5 * Data_volt(0)
Data_volt(1) = 10 * Data_volt(1)
Data_volt(2) = 25 * Data_volt(2)
Data_volt(3) = 50 * Data_volt(3)
Data_volt(4) = 100 * Data_volt(4)
Data_volt(5) = 200 * Data_volt(5)
Data_volt(6) = 300 * Data_volt(6)
Data_volt(7) = 500 * Data_volt(7)

'format datanya
Str_volt(0) = Fusing(data_volt(0) , "#.##")
Str_volt(1) = Fusing(data_volt(1) , "##.#")
Str_volt(2) = Fusing(data_volt(2) , "##.#")
Str_volt(3) = Fusing(data_volt(3) , "##.#")
Str_volt(4) = Fusing(data_volt(4) , "###.")
Str_volt(5) = Fusing(data_volt(5) , "###.")
Str_volt(6) = Fusing(data_volt(6) , "###.")
Str_volt(7) = Fusing(data_volt(7) , "###.")

'tampilkan data di LCD
For X = 0 To 3
X2 = X + 1
Y = X + 4

Locate X2 , 5 : Lcd Str_volt(x)
Locate X2 , 16 : Lcd Str_volt(y)
Next

Waitms 400

Loop
End
