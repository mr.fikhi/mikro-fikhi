$regfile = "m16def.dat"
$crystal = 16000000

'setting adc
Config Adc = Single , Prescaler = Auto
Start Adc

'setting lcd
Config Lcdpin = Pin , Db4 = Portc.4 , Db5 = Portc.5 , Db6 = Portc.6 , Db7 = Portc.7 , E = Portc.2 , Rs = Portc.1
Config Lcd = 20 * 4
Cursor Off Noblink
Cls

'definisi variable
Dim Vs As Single
Dim Data_adc As Single

Do

'menampilkan LCD
Locate 1 , 1 : Lcd "Voltmeter AC - Trafo"

'get data ADC
Data_adc = Getadc(0)

'konversi ke tegangan asli
Data_volt(0) = 5 * Data_volt(0)

'format datanya
Str_volt(0) = Fusing(data_volt(0) , "#.##")

'tampilkan data di LCD

Locate 3 , 5 : Lcd "Vs=" ; Str_volt(x)

Waitms 400

Loop
End