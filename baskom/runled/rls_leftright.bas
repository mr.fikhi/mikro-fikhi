$regfile = "m16def.dat"
$crystal = 16000000

'$sim

Config Portd = Output
Dim Datarun As Byte
Dim X As Integer

Do

Datarun = &HFE
For X = 0 To 7
Datarun = Datarun And &HFE
Portd = Datarun
Waitms 75
Rotate Datarun , Left
Next

Datarun = &H7F
For X = 0 To 7
Datarun = Datarun And &H7F
Portd = Datarun
Waitms 75
Rotate Datarun , Right
Next

Loop
End
