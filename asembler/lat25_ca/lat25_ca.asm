.nolist						; nolist
.include "m16def.inc" 	; include definition attiny2313
.list

.equ	d0	= 0b11000000
.equ	d1	= 0b11111001
.equ	d2	= 0b10100100
.equ	d3	= 0b10110000
.equ	d4	= 0b10011001
.equ	d5	= 0b10010010
.equ	d6	= 0b10000010
.equ	d7	= 0b11111000
.equ	d8	= 0b10000000
.equ	d9	= 0b10010000

.def	temp 	= r16		; register untuk var sementara
.def	temp2	= r24
.def	delay1 	= r17
.def	delay2 	= r18
.def	delay3 	= r19
.def	count	= r20

.cseg
.org	0000			; awal kode pada addr 0000

;init stack
ldi temp, 	low(ramend)		; simpan alamat akhir RAM ke temp
out SPL,	temp		; simpan alamat akhir RAM ke Stack Pointer-Low
ldi temp, 	high(ramend)		; simpan alamat akhir RAM ke temp
out SPH,	temp		; simpan alamat akhir RAM ke Stack Pointer-Low

;set DDRB
ldi temp,	0xFF		; atur semua pin pada PORTB menjadi output (0: input, 1:output)
out	DDRB,	temp		; atur DDRB menjadi temp=0xff
ldi temp,	0x00
out DDRC,	temp
ldi temp,	0xff
out PORTC,	temp
ldi count,	0x0

;program utama
awal:
in temp, PINC
in temp2, PINC

ori temp, 0b11111110 
cpi temp, 0b11111110
breq tambah_count

ori temp2, 0b01111111
cpi temp2, 0b01111111
breq kurang_count

rcall send_seg
rcall tunda
rjmp awal				; kembali ke label tunda

tambah_count:
cpi count, 9
breq reset_count
inc count
rcall send_seg
rcall tunda
rjmp awal

kurang_count:
cpi count, 0
breq set_count
dec count
rcall send_seg
rcall tunda
rjmp awal

reset_count:
ldi count, 0
rcall send_seg
rcall tunda
rjmp awal

set_count:
ldi count, 9
rcall send_seg
rcall tunda
rjmp awal

;subroutine delay
tunda:
ldi delay3,	0x50
tunggu0:
ldi	delay2,	0xff
tunggu1:
ldi	delay1,	0xff
tunggu2:
dec delay1
brne tunggu2
dec delay2
brne tunggu1
dec delay3
brne tunggu0
ret						; return ke program utama

send_seg:
cpi count, 0x0
breq send_0
cpi count, 0x1
breq send_1
cpi count, 0x2
breq send_2
cpi count, 0x3
breq send_3
cpi count, 0x4
breq send_4
cpi count, 0x5
breq send_5
cpi count, 0x6
breq send_6
cpi count, 0x7
breq send_7
cpi count, 0x8
breq send_8
cpi count, 0x9
breq send_9
ret

send_0:
ldi temp, d0
out PORTB, temp
ret

send_1:
ldi temp, d1
out PORTB, temp
ret

send_2:
ldi temp, d2
out PORTB, temp
ret

send_3:
ldi temp, d3
out PORTB, temp
ret

send_4:
ldi temp, d4
out PORTB, temp
ret

send_5:
ldi temp, d5
out PORTB, temp
ret

send_6:
ldi temp, d6
out PORTB, temp
ret

send_7:
ldi temp, d7
out PORTB, temp
ret

send_8:
ldi temp, d8
out PORTB, temp
ret

send_9:
ldi temp, d9
out PORTB, temp
ret

.exit					; akhir program
