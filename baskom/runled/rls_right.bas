$regfile = "m16def.dat"
$crystal = 16000000

'$sim

Config Portd = Output
Dim Datarun As Byte
Dim X As Integer

Do
Datarun = &H7F

For X = 0 To 7
Datarun = Datarun And &H7F
Portd = Datarun
Waitms 300
Rotate Datarun , Right
Next

Loop
End
