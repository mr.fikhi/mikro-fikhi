
.nolist						; nolist
.include "m16def.inc" 	; include definition attiny2313
.list

.equ	l1	= 0b01111111
.equ	l2	= 0b10111111
.equ	l3	= 0b11011111
.equ	l4	= 0b11101111
.equ	l5	= 0b11110111
.equ	l6	= 0b11111011
.equ	l7	= 0b11111101
.equ	l8	= 0b11111110

.def	temp 	= r16		; register untuk var sementara
.def	delay1 	= r17
.def	delay2 	= r18
.def	delay3 	= r19

.cseg
.org	0000			; awal kode pada addr 0000

;init stack
ldi temp, 	low(ramend)		; simpan alamat akhir RAM ke temp
out SPL,	temp		; simpan alamat akhir RAM ke Stack Pointer-Low
ldi temp, 	high(ramend)		; simpan alamat akhir RAM ke temp
out SPH,	temp		; simpan alamat akhir RAM ke Stack Pointer-Low

;set DDRB
ldi temp,	0xFF		; atur semua pin pada PORTB menjadi output (0: input, 1:output)
out	DDRB,	temp		; atur DDRB menjadi temp=0xff

;program utama
putar:
ldi temp,	l1
out PORTB, temp
rcall tunda
ldi temp,	l2
out PORTB, temp
rcall tunda
ldi temp,	l3
out PORTB, temp
rcall tunda
ldi temp,	l4
out PORTB, temp
rcall tunda
ldi temp,	l5
out PORTB, temp
rcall tunda
ldi temp,	l6
out PORTB, temp
rcall tunda
ldi temp,	l7
out PORTB, temp
rcall tunda
ldi temp,	l8
out PORTB, temp
rcall tunda

rjmp putar				; kembali ke label tunda

;subroutine delay
tunda:
ldi delay3,	0x05
tunggu0:
ldi	delay2,	0xFF
tunggu1:
ldi	delay1,	0xFF
tunggu2:
dec delay1
brne tunggu2
dec delay2
brne tunggu1
dec delay3
brne tunggu0
ret						; return ke program utama
.exit					; akhir program
