
.nolist						; nolist
.include "tn2313def.inc" 	; include definition attiny2313
.list

.equ	data	= 0x01
.def	temp 	= r16		; register untuk var sementara
.def	delay1 	= r17
.def	delay2 	= r18
.def	delay3 	= r19

.cseg
.org	0000			; awal kode pada addr 0000

;init stack
ldi temp, 	RAMEND		; simpan alamat akhir RAM ke temp
out SPL,	temp		; simpan alamat akhir RAM ke Stack Pointer-Low

;set DDRB
ldi temp,	0xFF		; atur semua pin pada PORTB menjadi output (0: input, 1:output)
out	DDRB,	temp		; atur DDRB menjadi temp=0xff

;program utama

ldi temp,	data		; isi data temp dengan data flip (0x0f)
out	PORTB,	temp		; atur output PORTB sesuai flip
;rcall tunda				; panggil fungsi delay
putar:
rol temp
out	PORTB,	temp		; atur output PORTB sesuai flop
;rcall tunda				; panggil fungsi delay
rjmp putar				; kembali ke label tunda

;subroutine delay
tunda:
ldi delay3,	0x01
tunggu0:
ldi	delay2,	0xFF
tunggu1:
ldi	delay1,	0xFF
tunggu2:
dec delay1
brne tunggu2
dec delay2
brne tunggu1
dec delay3
brne tunggu0
ret						; return ke program utama
.exit					; akhir program
