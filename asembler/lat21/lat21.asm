.nolist						; nolist
.include "m16def.inc" 	; include definition attiny2313
.list

.equ	d0	= 0b00111111
.equ	d1	= 0b00000110
.equ	d2	= 0b01011011
.equ	d3	= 0b01001111
.equ	d4	= 0b01100110
.equ	d5	= 0b01101101
.equ	d6	= 0b01111101
.equ	d7	= 0b00000111
.equ	d8	= 0b01111111
.equ	d9	= 0b01101111

.def	temp 	= r16		; register untuk var sementara
.def	delay1 	= r17
.def	delay2 	= r18
.def	delay3 	= r19

.cseg
.org	0000			; awal kode pada addr 0000

;init stack
ldi temp, 	low(ramend)		; simpan alamat akhir RAM ke temp
out SPL,	temp		; simpan alamat akhir RAM ke Stack Pointer-Low
ldi temp, 	high(ramend)		; simpan alamat akhir RAM ke temp
out SPH,	temp		; simpan alamat akhir RAM ke Stack Pointer-Low

;set DDRB
ldi temp,	0xFF		; atur semua pin pada PORTB menjadi output (0: input, 1:output)
out	DDRB,	temp		; atur DDRB menjadi temp=0xff

;program utama
putar:
ldi temp,	d0
out PORTB, temp
rcall tunda
ldi temp,	d1
out PORTB, temp
rcall tunda
ldi temp,	d2
out PORTB, temp
rcall tunda
ldi temp,	d3
out PORTB, temp
rcall tunda
ldi temp,	d4
out PORTB, temp
rcall tunda
ldi temp,	d5
out PORTB, temp
rcall tunda
ldi temp,	d6
out PORTB, temp
rcall tunda
ldi temp,	d7
out PORTB, temp
rcall tunda
ldi temp,	d8
out PORTB, temp
rcall tunda
ldi temp,	d9
out PORTB, temp
rcall tunda
rjmp putar				; kembali ke label tunda

;subroutine delay
tunda:
ldi delay3,	0x25
tunggu0:
ldi	delay2,	0xff
tunggu1:
ldi	delay1,	0xff
tunggu2:
dec delay1
brne tunggu2
dec delay2
brne tunggu1
dec delay3
brne tunggu0
ret						; return ke program utama
.exit					; akhir program
