$regfile = "m32def.dat"
$crystal = 16000000
$baud = 9600

'setting base array dan adc
Config Base = 0
Config Adc = Single , Prescaler = Auto
Config Portb = Output
Start Adc

'setting lcd
Config Lcdpin = Pin , Db4 = Portc.4 , Db5 = Portc.5 , Db6 = Portc.6 , Db7 = Portc.7 , E = Portc.2 , Rs = Portc.1
Config Lcd = 20 * 4
Cursor Off Noblink
Cls

'definisi variable
Dim X As Integer
Dim In_char As String * 1
Dim Data_adc(8) As Word

Locate 1 , 1 : Lcd "AAAAAAAAAAAAAAAAAAAA"
Locate 2 , 1 : Lcd "AAAAAAAAAAAAAAAAAAAA"
Locate 3 , 1 : Lcd "AAAAAAAAAAAAAAAAAAAA"
Locate 4 , 1 : Lcd "AAAAAAAAAAAAAAAAAAAA"

Print "HELLO TERMINAL!!!!!!!!"

Do

In_char = Waitkey()
Print In_char

If In_char = "q" Or In_char = "Q" Then

For X = 0 To 7
Data_adc(x) = Getadc(x)
Next

Print "[" ;

For X = 0 To 6
Print Data_adc(x) ; ":" ;
Next
Print Data_adc(7) ;

Print "]"
End If

If In_char = "A" Then
Portb.0 = 1
Elseif In_char = "a" Then
Portb.0 = 0
End If

If In_char = "S" Then
Portb.1 = 1
Elseif In_char = "s" Then
Portb.1 = 0
End If

If In_char = "D" Then
Portb.2 = 1
Elseif In_char = "d" Then
Portb.2 = 0
End If

If In_char = "F" Then
Portb.3 = 1
Elseif In_char = "f" Then
Portb.3 = 0
End If

If In_char = "G" Then
Portb.4 = 1
Elseif In_char = "g" Then
Portb.4 = 0
End If

If In_char = "H" Then
Portb.5 = 1
Elseif In_char = "h" Then
Portb.5 = 0
End If

If In_char = "J" Then
Portb.6 = 1
Elseif In_char = "j" Then
Portb.6 = 0
End If

If In_char = "K" Then
Portb.7 = 1
Elseif In_char = "k" Then
Portb.7 = 0
End If

Loop
End