$regfile = "m16def.dat"
$crystal = 16000000

Config Base = 0
Config Portd = Output

Dim Seg(10) As Byte
Dim X As Integer

Seg(0) = &H3F
Seg(1) = &H06
Seg(2) = &H5B
Seg(3) = &H4F
Seg(4) = &H66
Seg(5) = &H6D
Seg(6) = &HFD
Seg(7) = &H07
Seg(8) = &H7F
Seg(9) = &H6F

Do

For X = 0 To 9
Portd = Not Seg(x)
Waitms 300
Next

For X = 1 To 8
Portd = Not Seg(9 - X)
Waitms 300
Next

Loop

End