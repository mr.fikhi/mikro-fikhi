$regfile = "M16def.dat"
$crystal = 16000000

Config Base = 0
'PORTB sebagai output 7segment
Config Portb = Output
'PD.7 -> PD.4 OUTPUT
'PD.3 -> PD.0 INPUT
Ddrd = &HF0
Portd = &HFF

Dim Hasil As Integer
Dim Seg(16) As Byte

Seg(0) = &H3F
Seg(1) = &H06
Seg(2) = &H5B
Seg(3) = &H4F
Seg(4) = &H66
Seg(5) = &H6D
Seg(6) = &HFD
Seg(7) = &H07
Seg(8) = &H7F
Seg(9) = &H6F
Seg(10) = &H77
Seg(11) = &H7C
Seg(12) = &H58
Seg(13) = &H5E
Seg(14) = &H79
Seg(15) = &H71

Do
Portd.7 = 0
Portd.6 = 1
Portd.5 = 1
PORTD.4 = 1
If PIND.3 = 0 Then
Hasil = 1
Elseif Pind.2 = 0 Then
Hasil = 2
Elseif Pind.1 = 0 Then
Hasil = 3
Elseif Pind.0 = 0 Then
Hasil = 10
End If

Portd.7 = 1
Portd.6 = 0
Portd.5 = 1
PORTD.4 = 1
If Pind.3 = 0 Then
Hasil = 4
Elseif Pind.2 = 0 Then
Hasil = 5
Elseif Pind.1 = 0 Then
Hasil = 6
Elseif Pind.0 = 0 Then
Hasil = 11
End If

Portd.7 = 1
Portd.6 = 1
Portd.5 = 0
PORTD.4 = 1
If Pind.3 = 0 Then
Hasil = 7
Elseif Pind.2 = 0 Then
Hasil = 8
Elseif Pind.1 = 0 Then
Hasil = 9
Elseif Pind.0 = 0 Then
Hasil = 12
End If

Portd.7 = 1
Portd.6 = 1
Portd.5 = 1
Portd.4 = 0
If Pind.3 = 0 Then
Hasil = 14
Elseif Pind.2 = 0 Then
Hasil = 0
Elseif Pind.1 = 0 Then
Hasil = 15
Elseif Pind.0 = 0 Then
Hasil = 13
End If

Portb = Seg(hasil)

Waitms 100

Loop
End