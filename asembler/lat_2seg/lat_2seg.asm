;=========COUNTER-99============================
;program ini membuat counter-up angka seven segmen dari 0 - 99
;dua seven segmen pararel pada portd, portb0(LSB) & 
;portb1(MSB) sebagai pengendali
;oleh   		: Ardi winoto
;nama file	: L4_09.asm 		up date: 13 juli 2006
;==================================================
.nolist		;list file untuk include file tidak perlu ditampilkan
.include "m16def.inc" ;includ file di seting AVR Studio4 folder		
.list
;----------inisialisasi konstanta dan register-------------------------
;cc euy
.equ	sv0=0b00111111	;0 
.equ	sv1=0b00000110	;1
.equ	sv2=0b01011011	;2
.equ	sv3=0b01001111	;3
.equ	sv4=0b01100110	;4
.equ	sv5=0b01101101	;5
.equ	sv6=0b01111101	;6 
.equ	sv7=0b00000111	;7
.equ	sv8=0b01111111	;8
.equ	sv9=0b01101111	;9
.def	temp=r16
.def	delay=r17
.def	loop=r20
.def	disply=r22
.def	jml_data=r23
;====== program utama ============================
.cseg	
.org	0000	;awal kode program pada alama 0000
;----------inisialisasi stack-----------------------
	ldi temp, high(RAMEND)
	out SPH, temp
	ldi	temp,low(RAMEND)	;copy alamat ram terakhir ke temp
	out	SPL,temp	;tuliskan ke register stack
;----------inisialisasi port-----------------------
	ldi	temp,0b11111111;set semua bit register temp
	out	ddrd,temp	;tuliskan ke register DDRD
	out	ddrb,temp	;tuliskan ke register DDRB	
	out ddrc,temp

;----------tabel LSB counter-----------------------
Sv_LSB:
.DB 0x3f,0x06,0x5b,0x4f,0x66,0x6d,0x7d,0x07,0x7f,0x6f
;---------looping MSB-----------------------
counter99:			;label counter99
	ldi	temp,sv0	;isi temp dengan sv0			
	rcall	tampil		;panggil subruitn tampil
	ldi	temp,sv1	;isi temp dengan sv0		
	rcall	tampil		;panggil subruitn tampil
	ldi	temp,sv2	;isi temp dengan sv0
	rcall	tampil		;panggil subruitn tampil
	ldi	temp,sv3	;isi temp dengan sv0
	rcall	tampil		;panggil subruitn tampil
	ldi	temp,sv4	;isi temp dengan sv0
	rcall	tampil		;panggil subruitn tampil
	ldi	temp,sv5	;isi temp dengan sv0
	rcall	tampil		;panggil subruitn tampil
	ldi	temp,sv6	;isi temp dengan sv0
	rcall	tampil		;panggil subruitn tampil
	ldi	temp,sv7	;isi temp dengan sv0
	rcall	tampil		;panggil subruitn tampil
	ldi	temp,sv8	;isi temp dengan sv0
	rcall	tampil		;panggil subruitn tampil
	ldi	temp,sv9	;isi temp dengan sv0
	rcall	tampil		;panggil subruitn tampil
	rjmp	counter99	;lompat ke label counter99
;=====SUBRUTIN TAMPIL=====================
tampil:	ldi	loop,0xff	;isi loop dengan 0xff untuk pause loop
	ldi	disply,0b11111101
;isi disply dengan 0xfd untuk control output 		
	ldi	jml_data,10 	
;isi jml_data dengan 10 untuk looping angka tabel
	ldi	zl,low(Sv_LSB *2) ; inisialisasi pointer ZL 
putar:			;label putar untuk looping angka LSB
;+++++Untuk mempause sesaat+++++++++++++++++
pause:	out	portd,temp	;tuliskan temp ke portd
	out	portb,disply	;tuliskan isi disply ke portb
	com	disply		;balikan isi disply untuk control LSB
	rcall	tunda	;panggil subrutin tunda untuk delay sesaat
	lpm		;bca data tabel yang ditunjuk Z dan copy ke R0
	out	portb,disply	;tuliskan isi disply ke portb
	out	portc,R0	;tuliskan isi R0 ke portd
	com	disply	;balikan isi disply untuk control MSB
	rcall	tunda	;panggil subrutin tunda untuk delay sesaat
	dec	loop	;kurangi isi loop dengan satu
	brne	pause	;lompat ke pause jika belum nol
;+++++ Akhir pause sesaat+++++++++++++++++++
	inc	zl	;naikan pointer Z 
	dec	jml_data;kurangi isi jml_data dengan satu
	brne	putar	;lompat ke putar jika belum nol
	ret		;jika sudah nol, tinggalkan subrutin tampil
;===== AKHIR SUBRUTIN TAMPIL==================
tunda:	ldi	delay,0xff	;subrutin tunda, isi delay dengan 0xff	
tunggu:dec	delay		;kurangi isi delay dengan satu		
	brne	tunggu	;jika belum nol lompat ke label tunggu
	ret		;jika sudah nol, tinggalkan subrutin tunda
 .exit
